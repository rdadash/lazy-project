import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from "./components/admin/admin.component";
import { AboutComponent } from "./components/about/about.component";
import { HomeComponent } from "./components/home/home.component";

const routes: Routes = [
  { path: '', component: AdminComponent,
    children: [
      { path: 'home', component: HomeComponent  },
      { path: 'about', component: AboutComponent  },
      { path: '', redirectTo: '/admin/home', pathMatch: 'full' }
    ]  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
